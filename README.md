# DBLUG bot-source repository. 

Welcome to the bot code-repository of (just about) all bots running in the (DB)-LUG IRC channel. 

## Current bots in repository: 
* ReminderBot: A 'secretary bot' that will accept timed requests for reminders and remind users in-channel when the date and time arrives. 