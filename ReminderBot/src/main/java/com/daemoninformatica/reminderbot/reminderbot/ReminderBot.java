/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.reminderbot;

import com.daemoninformatica.reminderbot.data.exceptions.ReminderBotException;
import com.daemoninformatica.reminderbot.data.Reminder;
import com.daemoninformatica.reminderbot.handlers.HelpCommandHandler;
import com.daemoninformatica.reminderbot.handlers.ListRemindersHandler;
import com.daemoninformatica.reminderbot.handlers.RemindCommandHandler;
import com.daemoninformatica.reminderbot.handlers.RemindMeCommandHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class ReminderBot extends PircBot
{
    private final   int             REMINDER_LIMIT  = 30;
    private final   List<Reminder>  listOfReminders = new ArrayList<>();
    private final   String          server;
    private final   int             port;
    private final   String          channel;
    private         Runnable        deadlineMonitor;
    
    public ReminderBot(final String name, final String server, final int port, 
            final String channel) 
    {
        super();
        System.out.println("Initializing ReminderBot...");
        
        this.server     = server;
        this.port       = port;
        this.channel    = channel;
        
        setName(name);
        System.out.println("reminderBot " + name + " initialized");
    }
     
    private void startDeadlineThread()
    {
        System.out.println("Starting deadline monitoring thread.");
        deadlineMonitor = new DeadlineMonitoringThread(this, channel, 
                listOfReminders);
        
        Thread t = new Thread(deadlineMonitor);
        
        t.start();        
        System.out.println("Deadline monitoring thread started.");
    }

    private void startIRCConnection()  throws IOException, IrcException
    {
        System.out.println("Starting IRC connection and joining channel...");
        
        setVerbose(true);
        connect(server, port);
        joinChannel(channel);
        
        System.out.println("Done starting IRC connection.");
    }
    
    public void start() throws ReminderBotException
    {
        System.out.println("Starting bot...");
        
        try
        {
            startIRCConnection();
            startDeadlineThread();
            
            System.out.println("Done starting bot.");
        }
        catch(IOException | IrcException e)
        {
            throw new ReminderBotException("Failed starting bot", e);
        }
    }
    
    private boolean evaluateReminderListLimit()
    {
        if(listOfReminders.size() >= REMINDER_LIMIT)
        {
            Reminder nextReminder = listOfReminders.get(0);
            
            sendMessage(channel, "I'm sorry, I have reached my limit of " + 
                    REMINDER_LIMIT + " messages to store. ");
            
            sendMessage(channel, "Try again after " + 
                    nextReminder.getDeadline());
            
            return false;
        }
        
        return true;
    }
    
    private void handleCommand(final String sender, final String command)
    {
        // BaseCommandHandler handler = null;
        
        try
        {
            if(command.startsWith("!help"))
            {
                new HelpCommandHandler(this, sender, command, channel);
            }

            if(command.split(" ")[0].equals("!remind") && evaluateReminderListLimit())
            {
                RemindCommandHandler handler = new RemindCommandHandler(this, 
                        sender, command, channel);
                
                this.listOfReminders.add(handler.getReminder());
            }
            
            if(command.split(" ")[0].equals("!reminder_list"))
            {
                new ListRemindersHandler(this, sender, command, channel, 
                        listOfReminders);
            }
            
            if(command.split(" ")[0].equals("!remindme") && evaluateReminderListLimit())
            {
                RemindMeCommandHandler handler = new 
                    RemindMeCommandHandler(this, sender, command, channel);
                
                this.listOfReminders.add(handler.getReminder());
            }
        }
        catch(ReminderBotException r_e)
        {
            System.err.println("Caught exception trying to handle a remindme: " 
                    + r_e.getMessage());
        }
        
        // After each command, the list is potentially updated. Sort it by 
        // deadline ascending, so we only have to monitor the first item each 
        // cycle. 
        this.listOfReminders.sort(Comparator.comparing(reminder -> 
                reminder.getDeadline()));
    }
    
    @Override
    protected void onMessage(String channel, String sender, String login, 
            String hostname, String message) 
    {
        if(message.startsWith("!")) handleCommand(sender, message);
    }
}
