/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import com.daemoninformatica.reminderbot.data.Reminder;
import com.daemoninformatica.reminderbot.data.exceptions.ReminderBotException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class RemindMeCommandHandler extends BaseReminderCommandHandler
{
    private final String        recipient; 
    private final String        reminder;
    private final LocalDateTime deadline;

    @Override
    public final void usage() 
    {
        String channel = getChannel();
        
        if(channel == null)
            System.err.println("I don't appear to have a channel name at this "
                    + "point..");
        
        System.out.println("Sending usage to channel: " + channel);
        
        bot.sendMessage(channel, " >> !remindme usage: ");
        bot.sendMessage(channel, " >> !remindme [yyyy-mm-dd] [hh:mm:ss] "
                + "[message]");
    }

    public RemindMeCommandHandler(final PircBot bot, final String sender, 
            final String message, final String channel)  throws 
                ReminderBotException
    {
        super(bot, sender, message, channel);
        
        System.out.println("Executing RemindMeCommandHandler with message '" + 
                message + "'....");
        
        // List<String> parameters = new ArrayList<String>();
        List<String> parameters = Arrays.stream(message.split(" "))
                .collect(Collectors.toList());

        this.recipient = sender;
        
        if(parameters.size() < 4)
        {
            usage();
            throw new ReminderBotException("message '" + message + 
                    "' has insufficient parameters");
        }
        
        this.deadline = parseTimeFromMessage(parameters);
        this.reminder = parseReminderFromMessage(parameters, 3);
        
        System.out.println("Done processing remindMe command '" + message + 
                "'.");
    }

    
    public Reminder getReminder() 
    {
        return super.getReminder(recipient, deadline, reminder);
    }
}

