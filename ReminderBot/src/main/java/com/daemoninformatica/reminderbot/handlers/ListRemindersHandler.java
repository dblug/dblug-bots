/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import com.daemoninformatica.reminderbot.data.Reminder;
import java.util.List;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class ListRemindersHandler extends BaseCommandHandler
{
    private final List<Reminder> reminder_list;
    
    public ListRemindersHandler(PircBot bot, String sender, String message, 
            String channel, final List<Reminder> reminder_list) 
    {
        super(bot, sender, message, channel);
        
        this.reminder_list = reminder_list;
        execute();
    }

    @Override
    public void usage() {
        /*
        No parameters. So no usage function. 
        */
    }
    
    private void execute()
    {
        if(reminder_list.isEmpty())
        {
            bot.sendMessage(getChannel(), "There are no messages in the queue.");
            return;
        }
        
        bot.sendMessage(getChannel(), "The following reminders are currently "
                + "stored: ");
        
        for(Reminder reminder : reminder_list)
            bot.sendMessage(getChannel(), reminder.toString());
    }
}
