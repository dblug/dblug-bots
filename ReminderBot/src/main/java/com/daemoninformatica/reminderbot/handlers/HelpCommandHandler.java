/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class HelpCommandHandler extends BaseCommandHandler
{

    public HelpCommandHandler(PircBot bot, String sender, String message, 
            String channel) 
    {
        super(bot, sender, message, channel);
        
        execute();
    }

    @Override
    public void usage() {
        /* Isn't going to be called, since the entire point is that this is 
        * going to tell you how stuff works. 
        */
    }
    
    private void execute()
    {
        bot.sendMessage(getChannel(), "I am a date-time based reminder-bot. ");
        bot.sendMessage(getChannel(), "Based on a date, a time and other "
                + "function specific parameters I ");
        
        bot.sendMessage(getChannel(), "keep a ledger of messages to display on "
                + "specific times. ");
        
        bot.sendMessage(getChannel(), "Currently Implemented functions: ");
        bot.sendMessage(getChannel(), " !help    Show this help.");
               
        bot.sendMessage(getChannel(), " !remind    Command to remind another "
                + "user of something at a specified point in time.");
        
        bot.sendMessage(getChannel(), " !reminder_list    Dump list of queued "
                + "reminders");
        
        bot.sendMessage(getChannel(), " !remindme    Command to remind the user"
                + " of something at a specified point in time.");
        
        bot.sendMessage(getChannel(), "For more specific information about each"
                + " function, type the command without parameters.");
    }
}
