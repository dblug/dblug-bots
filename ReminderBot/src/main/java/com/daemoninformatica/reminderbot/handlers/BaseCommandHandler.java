/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public abstract class BaseCommandHandler 
{
    protected   final PircBot   bot;
    private     final String    sender;
    private     final String    message;
    private     final String    channel;

    public BaseCommandHandler(final PircBot bot, final String sender, final 
            String message, final String channel) 
    {
        this.bot        = bot;
        this.sender     = sender;
        this.message    = message;
        this.channel    = channel;
    }
    
    public abstract void usage();

    public String getSender() {
        return sender;
    }

    public String getMessage() {
        return message;
    }

    public String getChannel() {
        return channel;
    }   
}
