/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import com.daemoninformatica.reminderbot.data.Reminder;
import com.daemoninformatica.reminderbot.data.exceptions.ReminderBotException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public abstract class BaseReminderCommandHandler extends BaseCommandHandler
{

    public BaseReminderCommandHandler(PircBot bot, String sender, String message, String channel) {
        super(bot, sender, message, channel);
    }

    protected LocalDateTime parseTimeFromMessage(final List<String> parameters)
            throws ReminderBotException
    {
        try
        {
            String date         = parameters.get(1);
            String time         = parameters.get(2);
            String dateElem[]   = date.split("-");
            String timeElem[]   = time.split(":");

            LocalDateTime calculatedDeadline = LocalDateTime.of(
                    Integer.valueOf(dateElem[0]),
                    Integer.valueOf(dateElem[1]),
                    Integer.valueOf(dateElem[2]),
                    Integer.valueOf(timeElem[0]),
                    Integer.valueOf(timeElem[1]),
                    Integer.valueOf(timeElem[2])
            );


            LocalDateTime dtNow = LocalDateTime.now(ZoneId.of("CET"));

            System.out.println("dtNow: " + dtNow);
            System.out.println("calculatedDateTime: " + calculatedDeadline);

            if(calculatedDeadline.isBefore(dtNow))
            {
                bot.sendMessage(getChannel(), "Timestamp " + date + " " + time +
                        " is in the past...");

                throw new ReminderBotException("Timestamp was in the past.");
            }

            return calculatedDeadline;
        }
        catch(IndexOutOfBoundsException i_e)
        {
            throw new ReminderBotException("Error parsing time for 'RemindMe'.",
                    i_e);
        }
    }

    protected String parseReminderFromMessage(final List<String> parameters, int startElement)
    {
        String reminder = "";

        for(int i = startElement; i < parameters.size(); i++)
        {
            reminder += parameters.get(i);
            reminder += " ";
        }

        return reminder;
    }

    public Reminder getReminder(final String screenname, final LocalDateTime deadline, final String reminder)
    {
        Reminder reminderObj = new Reminder(screenname, deadline, reminder);

        bot.sendMessage(getChannel(), "I created the following reminder: " +
                reminderObj.toString());

        return reminderObj;
    }

}
