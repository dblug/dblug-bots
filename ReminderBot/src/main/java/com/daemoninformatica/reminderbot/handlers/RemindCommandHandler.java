/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.handlers;

import com.daemoninformatica.reminderbot.data.Reminder;
import com.daemoninformatica.reminderbot.data.exceptions.ReminderBotException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class RemindCommandHandler extends BaseReminderCommandHandler
{
    private String          screenname;
    private LocalDateTime   deadline;
    private String          reminder;
    
    public RemindCommandHandler(PircBot bot, String sender, String message, 
            String channel) throws ReminderBotException
    {
        super(bot, sender, message, channel);
        execute();
    }
    
    private void execute() throws ReminderBotException
    {
        List<String> parameters = Arrays.stream(getMessage().split(" "))
                .collect(Collectors.toList());

        if(parameters.size() < 5)
        {
            usage();
            throw new ReminderBotException("message '" + getMessage() + 
                    "' has insufficient parameters");
        }
            
        // parse out date and time. 
        deadline = parseTimeFromMessage(parameters);
        
        // parse out recipient. 
        screenname = parameters.get(3);
        
        // parse out message. 
        reminder = parseReminderFromMessage(parameters, 4);
        
    }

    public Reminder getReminder()
    {
        return super.getReminder(screenname, deadline, reminder);
    }
    
    @Override
    public void usage() {
        bot.sendMessage(getChannel(), "Usage: !remind [yyyy-mm-dd] [hh:mm:ss] "
                + "[screenname] [message]");
    }
    
}
