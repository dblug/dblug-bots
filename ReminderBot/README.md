# 	ReminderBot: 

## Description: 
A bot that will remind people that ask for it on specific times and dates. 
Written by DaemonInformatic.

## Commands: 
* !help: Show a list of commands available for this bot. 
* !remind: Remind a person about something, on a specific date and time. 
* !remindme: Remind the person that asks for a reminder about something, on a specific date and time. 
* !reminder_list: Show a list of current reminders, queued for delivery. 

## Installation:  
The Bot is written in Java (based on PircBot) and is easily built with 'maven install'. This generates an executable jar-file which can then be started with: 

java -jar [filename.jar]


## ToDo: 
This is a list of things to do to make it even prettier: 
* More robust parsing of (potentially wrong (order / content) of) parameters. 

* Delete reminder command. To be able to remove reminders. 

* Private chat window to the bot, to create a 'personal assistant instance' which receives reminder commands to only show in that thread. 

* Make reminders persistent. So that, when the bot drops out or disconnects and has to be restarted, (relevant) reminders are still available. 

* Add argparser to override connection, name and channel parameters that are currently hardcoded. 
